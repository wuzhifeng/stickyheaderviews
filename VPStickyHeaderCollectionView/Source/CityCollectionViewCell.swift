//
//  CityCollectionViewCell.swift
//  VPStickyHeaderCollectionView
//
//  Created by Deepak VP on 7/5/14.
//  Copyright (c) 2014 Deepak VP. All rights reserved.
//

import UIKit

class CityCollectionViewCell: UICollectionViewCell {

  /*required init(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
  }*/

  @IBOutlet var cityNameLabel : UILabel?

  func setUpCellWithCityName(cityName: String) {
      cityNameLabel!.text = cityName
  }
}
